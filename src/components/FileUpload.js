
import React from 'react';
import {BaseComponent} from "./BaseComponent";
import {ProgressBar} from "./loading/ProgressBar";

/**
 * expected props:
 *   chooseLabel : text for the prompt for the input field
 *   multiple
 *   access
 *   disabled
 *   maxFileSize
 */
export class FileUpload extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            ...this.state,
            files: [],
            msgs: []
        };

        this.onFileSelect = this.onFileSelect.bind(this);
        this.onDragEnter = this.onDragEnter.bind(this);
        this.onDragOver = this.onDragOver.bind(this);
        this.onDragLeave = this.onDragLeave.bind(this);
        this.onDrop = this.onDrop.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onSimpleUploaderClick = this.onSimpleUploaderClick.bind(this);
        this.upload = this.upload.bind(this);
        this.clear = this.clear.bind(this);
    }
    render() {
        console.log('FileUpload.render()', this.state);
        let buttonClassName = classNames('ui-button ui-fileupload-choose ui-widget ui-state-default ui-corner-all ui-button-text-icon-left', {'ui-fileupload-choose-selected': this.hasFiles()});
        let iconClassName = classNames('ui-button-icon-left fa fa-upload');
        let chooseLabel = this.props.chooseLabel || 'Choose file...';
        let progressBar = this.state.progress > 0 ? (
            <ProgressBar value={this.state.progress} showValue={true} />
        ) : '';
        let messages = this.state.msgs.length > 0 ? (
            <div>
                {this.state.msgs.map(function(msg) {
                    return (<div className={msg.severity}>{msg.summary} <div>{msg.detail}</div></div>);
                })}
            </div>

        ) : '';

        return (
            <span className={buttonClassName} onMouseUp={this.onSimpleUploaderClick}>
                <span className={iconClassName} />
                <span className="ui-button-text ui-clickable">
                    {this.hasFiles() ? this.state.files[0].name : chooseLabel}
                </span>
                <input
                    ref={(el) => this.fileInput = el} type="file" multiple={this.props.multiple}
                    accept={this.props.accept} disabled={this.props.disabled}
                    onChange={this.onFileSelect} onFocus={this.onFocus} onBlur={this.onBlur}
                />
                {progressBar}
                {messages}
            </span>
        );
    }

    onFileSelect(event) {
        this.setState({msgs:[]});
        let files = event.dataTransfer ? event.dataTransfer.files : event.target.files;
        for (let file of files) {
            console.log(file);
            if (!this.isFileSelected(file)) {
                if (this.validate(file)) {
                    if (this.isImage(file)) {
                        file.objectURL = window.URL.createObjectURL(file);
                    }

                    this.setState({files: [...this.state.files, file]}, () => {
                        if (this.hasFiles() && this.props.auto) {
                            this.upload();
                        }
                    });
                }
            }
        }

        if(this.props.onSelect) {
            this.props.onSelect({originalEvent: event, files: this.state.files});
        }

        this.clearInputElement();

        if(this.props.mode === 'basic') {
            this.fileInput.style.display = 'none';
        }
    }
    onDragEnter(event) {
        if(!this.props.disabled) {
            event.stopPropagation();
            event.preventDefault();
        }
    }
    onDragOver(event) {
        if(!this.props.disabled) {
            DomHandler.addClass(this.content, 'ui-fileupload-highlight');
            event.stopPropagation();
            event.preventDefault();
        }
    }
    onDragLeave(event) {
        if(!this.props.disabled) {
            DomHandler.removeClass(this.content, 'ui-fileupload-highlight');
        }
    }
    onDrop(event) {
        if(!this.props.disabled) {
            DomHandler.removeClass(this.content, 'ui-fileupload-highlight');
            event.stopPropagation();
            event.preventDefault();

            let files = event.dataTransfer ? event.dataTransfer.files : event.target.files;
            let allowDrop = this.props.multiple||(files && files.length === 1);

            if(allowDrop) {
                this.onFileSelect(event);
            }
        }
    }
    onFocus(event) {
        DomHandler.addClass(event.currentTarget.parentElement, 'ui-state-focus');
    }
    onBlur(event) {
        DomHandler.removeClass(event.currentTarget.parentElement, 'ui-state-focus');
    }
    onSimpleUploaderClick() {
        if(this.hasFiles()) {
            this.upload();
            this.fileInput.style.display = 'inline';
        }
    }
    upload() {
        this.setState({msgs:[]});
        let xhr = new XMLHttpRequest();
        let formData = new FormData();

        this.onEvent('beforeUpload', {xhr: xhr});

        for (let file of this.state.files) {
            formData.append(this.props.name, file, file.name);
        }

        xhr.upload.addEventListener('progress', (event) => {
            if (event.lengthComputable) {
                const newProgress = Math.round((event.loaded * 100) / event.total);
                this.setState({progress: newProgress});
                this.onEvent('progress', {value: newProgress});
            }
        });

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                this.setState({progress: 0});
                if (xhr.status >= 200 && xhr.status < 300) {
                    this.onEvent('uploadCompleted', {xhr: xhr, files: this.state.files});
                } else {
                    this.onEvent('uploadFailed', {xhr: xhr, files: this.state.files});
                }
                this.clear();
            }
        };

        xhr.open('POST', this.props.url, true);

        this.onEvent('beforeSend', {xhr: xhr, formData: formData});

        xhr.withCredentials = this.props.withCredentials;

        xhr.send(formData);
    }
    clear() {
        this.setState({files:[]});
        this.onEvent('uploadCleared');
        this.clearInputElement();
    }

    hasFiles() {
        return this.state.files && this.state.files.length > 0;
    }
    isImage(file) {
        return /^image\//.test(file.type);
    }
    remove(index) {
        this.clearInputElement();
        let currentFiles = [...this.state.files];
        currentFiles.splice(index, 1);
        this.setState({files: currentFiles});
    }
    clearInputElement() {
        this.fileInput.value = '';
    }
    formatSize(bytes) {
        if (bytes === 0) {
            return '0 B';
        }
        let k = 1000,
            dm = 3,
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
    isFileSelected(file) {
        for (let sFile of this.state.files) {
            if ((sFile.name + sFile.type + sFile.size) === (file.name + file.type + file.size))
                return true;
        }
        return false;
    }
    validate(file) {
        // TODO check that file name matches this.props.access

        if (this.props.maxFileSize && file.size > this.props.maxFileSize) {
            let messages = this.state.msgs.slice();
            messages.push({
                severity: 'error',
                summary: "Size of file {0} is larger than allowed maximum".replace('{0}', file.name),
                detail: "File size exceed {0}".replace('{0}', this.formatSize(this.props.maxFileSize))
            });
            this.setState({msgs: messages});
            return false;
        }

        return true;
    }
}