
import React from 'react';
import {BaseComponent} from "../BaseComponent";

export class LoadMore extends BaseComponent {
    render() {
        return this.props.more > 0 ? (
            <div className="load-more">
                <a onClick={this.props.fn}>{this.translate('load.more')}</a>
            </div>
        ) : (
            <div />
        );
    }

    getTranslations(language) {
        switch (language.toLowerCase()) {
            case 'ru':
                return {
                    'load.more': 'Ещё'
                };
                break;
            case 'de':
                return {
                    'load.more': 'Mehr anzeigen'
                };
                break;
            case 'en':
            default:
                return {
                    'load.more': 'Load more'
                };
        }
    }
}