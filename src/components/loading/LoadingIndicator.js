
import React from 'react';
import {BaseComponent} from "../BaseComponent";

export class LoadingIndicator extends BaseComponent {
    render() {
        return this.props.loading ? (
            <div className="loading-indicator__loading">{this.translate('loading')}</div>
        ) : (
            <div className="loading-indicator__loaded">{this.props.children}</div>
        );
    }

    getTranslations(language) {
        switch (language.toLowerCase()) {
            case 'ru':
                return {
                    'loading': 'Идёт загрузка...'
                };
                break;
            case 'de':
                return {
                    'loading': 'de: Loading...'
                };
                break;
            case 'en':
            default:
                return {
                    'loading': 'Loading...'
                };
        }
    }
}