
import React from 'react';
import {BaseComponent} from "../BaseComponent";

/**
 * expected props:
 *   className : string or array  - optional custom styles for the component
 *   style : object - optional inline styles for the component
 *   value : float - current progress percentage
 *   showValue : boolean - shows textual value of progress percentage on top of the bar
 */
export class ProgressBar extends BaseComponent {
    render() {
        const className = classNames('ui-progressbar ui-widget ui-widget-content ui-corner-all', this.props.className);
        const label = (this.props.showValue) ? (<div className="ui-progressbar-label" style={{display: this.props.value ? 'block' : 'none'}}>{this.props.value + '%'}</div>) : '';

        return (
            <div className={className} role="progressbar" aria-valuemin="0" aria-valuenow={this.props.value} aria-valuemax="100" style={this.props.style}>
                <div className="ui-progressbar-value ui-progressbar-value-animate ui-widget-header ui-corner-all" style={{width: this.props.value + '%', display: 'block'}} />
                {label}
            </div>
        );
    }
}