
import React from 'react';
import {BaseComponent} from "./BaseComponent";
import {LoadingIndicator} from "./loading/LoadingIndicator";
import {LoadMore} from "./loading/LoadMore";

export class BaseImageGallery extends BaseComponent {
    constructor(props) {
        super(props);
        const apiParams = this.props.apiParams || {};
        const preload = ('preload' in apiParams) ? parseInt(apiParams.preload) : 0;
        this.state = {
            ...this.state,
            loading: true,
            preload: preload,
            pages_loaded: 0,
            items_loaded: 0,
            list: [],
            more: 0,
            markup: ''
        };
        this.loadPage = this.loadPage.bind(this);
    }
    componentDidMount() {
        if (this.props.apiParams && this.props.apiMethod) {
            this.loadPage();
        } else if (this.props.images) {
            this.setState({loading: false, list: this.props.images});
        } else {
            console.log('BaseImageGallery.componentDidMount() is not instructed about getting images');
        }
    }
    componentDidUpdate() {
        // init or update gallery here
        if (this.state.pages_loaded === 1 || this.state.preload > 0) {
            this.initGallery();
        } else if (this.state.pages_loaded > 0) {
            this.updateGallery();
        }
    }
    render() {
        let imagesDiv = (
            <div className="image-gallery__images">{this.state.list.map(this.renderItem)}</div>
        );

        return (
            <div className="image-gallery">
                {imagesDiv}
                <LoadingIndicator language={this.state.language} loading={this.state.loading}>
                    <LoadMore language={this.state.language} more={this.state.more} fn={this.loadPage}/>
                </LoadingIndicator>
            </div>
        );
    }

    renderItem(data) {
        console.log('BaseImageGallery.renderItem()', data);
        const imageData = {
            id: data.id,
            _i_: data._i_,
            src: 'si/' + data.id + '/height/400',
            author: data.author_alias
        };

        if (!('src' in imageData)) {
            return (<div className="image-gallery-item"/>);
        }

        const link = ('link' in imageData) ? imageData.link.trim() : '';
        const image = (link === '')
            ? (<img src={imageData.src}/>)
            : (<a href={link}><img src={imageData.src}/></a>);
        const author = ('author' in imageData)
            ? (
                <div className="image-gallery-item__author">
                    <p>{imageData.author}</p>
                </div>
            ) : '';

        return (
            <div className="image-gallery-item" key={imageData.id} data-supplier_image={imageData.id} data-seq_no={imageData._i_}>
                {image}
                {author}
            </div>
        );
    }
    getGallerySelector() {
        return '#' + this.props.galleryId + ' .image-gallery__images';
    }
    getSearchParams() {
        let params = this.props.apiParams || {};
        // overwrite page/preload with information from the state
        params.page = parseInt(this.state.pages_loaded) + 1;
        params.preload = this.state.preload;

        return params;
    }
    loadPage() {
        let vm = this;
        vm.setState({loading: true});
        const onSuccess = function (data) {
            const stateDelta = {
                loading: false,
                pages_loaded: parseInt(data.stats.lastPage),
                items_loaded: parseInt(vm.state.items_loaded) + parseInt(data.stats.fetched),
                list: vm.state.list.concat(data.list),
                more: parseInt(data.stats.more),
            };
            vm.setState(stateDelta);
        };
        const onFailure = function (data) {
            vm.setState({loading: false});
        };
        const onFinally = function(data) { // request might have failed
            if (vm.state.loading) {
                vm.setState({loading: false});
            }
        };
        jsonPost(this.props.apiMethod, this.getSearchParams(), onSuccess, onFailure, onFinally);
    }
    initGallery() {
        const galleryElement = $(this.getGallerySelector());
        console.log('BaseImageGallery.initGallery', galleryElement);
        if (galleryElement.length > 0) {
            const vm = this;
            galleryElement.justifiedGallery({
                border: 0,
                rowHeight: 240,
                maxRowHeight: false,
                cssAnimation: true,
                margins: 6,
                captions: false
            }).on('jg.complete', function (data) {
                if (vm.state.preload > 0) {
                    // scroll to the image that caused the preload
                    const element = galleryElement.find('.image-gallery-item[data-seq_no="' + vm.state.preload + '"]');
                    console.log('jg.complete element with seq_no=' + vm.state.preload, element);
                    if (element.length > 0) {
                        element.scrollIntoView();
                    }
                    // reset preload option if it was active, to prevent it from firing again
                    vm.state.preload = 0;
                }
            });
        }
    }
    updateGallery() {
        const galleryElement = $(this.getGallerySelector());
        console.log('BaseImageGallery.updateGallery', galleryElement);
        if (galleryElement.length > 0) {
            galleryElement.justifiedGallery('norewind');
        }
    }
}



