
import React, {Component} from 'react';

/**
 * expected props:
 *   language : 2-letter code like 'en', 'de', 'fr', 'it', 'es', 'ru', etc
 */
export class BaseComponent extends Component {
    constructor (props) {
        super(props);
        const language = props.language || this.getDefaultLanguage();
        const listeners = props.listeners || [];
        this.state = {
            listeners: listeners,
            language: language,
            i18n: this.getTranslations(language)
        };
    }

    onEvent(event, params) {
        // subclass if BaseComponent might override this method when it knows how to react to the event.
        //  otherwise, super.onEvent(cmd, params) will send the message further to any registered listeners.
        this.sendEvent(event, params);
    }
    sendEvent(event, params) {
        const listeners = [...this.state.listeners];
        listeners.forEach(function(listener) {
            if ('onEvent' in listener) {
                listener.onEvent(event, params);
            }
        });
    }
    getTranslations(language) {
        // when subclass of BaseComponent has its i18n strings, add them here
        //  e.g.: if (language == 'de') return { label1: "One" };
        return {};
    }
    getDefaultLanguage() {
        // by default, we expect default language to be defined in localStorage.
        //  when not found, just fallback to English
        return ('sessionLanguage' in localStorage) ? localStorage.getItem('sessionLanguage') : 'en';
    }
    translate(key) {
        const translations = this.state.i18n || {};
        return (key in translations) ? translations[key] : key;
    }
}