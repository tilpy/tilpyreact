
import React from 'react';
import {BaseComponent} from "./BaseComponent";

export class BaseContainer extends BaseComponent {
    sendEvent(event, params) {
        // Do not send further because we expect no listener for containers
        console.log('Unhandled event in BaseContainer', event, params);
    }
}