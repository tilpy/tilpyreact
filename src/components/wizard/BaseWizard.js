
import React from 'react';
import {BaseComponent} from "../BaseComponent";
import {WizardSteps} from "./WizardSteps";

export class BaseWizard extends BaseComponent {
    constructor(props) {
        super(props);
        const steps = this.getSteps() || [];
        this.state = {
            ...this.state,
            step: this.props.step || 0,
            lastStep: this.props.step || 0,
            complete: false
        };
    }
    render() {
        return this.state.complete ? this.renderComplete() : this.renderIncomplete();
    }

    renderComplete() {
        return (
            <div id="story-wizard-complete">
                <div className="row">
                    <div className="col-md-12">
                        Thank you.
                    </div>
                </div>
            </div>
        )
    }
    renderIncomplete() {
        return (
            <div id="story-wizard">
                <div className="row">
                    <div className="col-md-3">
                        <WizardSteps steps={this.getSteps()} currentStep={this.state.step} lastStep={this.state.lastStep} listeners={[this]} />
                    </div>
                    <div className="col-md-9">
                        {this.renderStep()}
                    </div>
                </div>
            </div>
        );
    }
    getSteps() {
        console.log('BaseWizard.getSteps() not implemented');
        return [];
    }
    renderStep() {
        console.log('BaseWizard.renderStep() not implemented');
        return <div/>;
    }
    onEvent(cmd, params) {
        if (cmd === 'goto') { // revisiting any of the steps that were already shown
            const step = params.step || 0;
            if (step <= this.state.lastStep && step !== this.state.step) {
                this.setState({step: step});
            }
        } else if (cmd === 'next') { // go to the next step for the first time
            const vm = this;
            const steps = this.getSteps();
            if (this.state.step === steps.length - 1) {
                this.submitStep((data) => {
                    console.log('callback on submitting last step');
                    // on last step, show confirmation
                    vm.setState({complete: true});
                });
            } else if (this.state.step <= this.state.lastStep) {
                console.log('not the last step yet');
                this.submitStep((data) => {
                    console.log('callback on submitting step ' + vm.state.step);
                    vm.setState({step: vm.state.step + 1});
                });
            }
        }
    }
    submitStep(callback) {
        console.log('BaseWizard.submitStep() not implemented');
    }
}

