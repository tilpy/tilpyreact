
import React from 'react';
import {BaseComponent} from "../BaseComponent";

export class WizardSteps extends BaseComponent {
    constructor(props) {
        super(props);
    }
    render() {
        const vm = this;
        const steps = [...this.props.steps];
        const currentStep = this.props.currentStep;
        const lastStep = this.props.lastStep;
        return (
            <div className="tilpy-wizard-steps">
                {steps.map(function(stepInfo, index) {
                    let styles = ['tilpy-wizard-step'];
                    if (index === currentStep) {
                        styles.push('tilpy-wizard-step-active')
                    } else if (index <= lastStep) {
                        styles.push('tilpy-wizard-step-available')
                    } else {
                        styles.push('tilpy-wizard-step-unavailable')
                    }
                    return (
                        <div key={index} className={styles.join(' ')} onClick={() => vm.onEvent('goto', {step: index})}>
                            {stepInfo.label}
                        </div>
                    );
                })}
            </div>
        );
    }
}