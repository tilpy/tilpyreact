
import React from 'react';
import {BaseComponent} from "../BaseComponent";

export class PeriodicElement extends BaseComponent {
    constructor(props) {
        super(props);
        this.onClickFn = this.onClickFn.bind(this);
    }

    render() {
        const _style = {
            width: '100px',
            height: '100px',
            textAlign: 'center',
            borderRadius: '8px',
            borderColor: 'grey',
            borderWidth: '2px',
            borderStyle: 'solid',
            fontSize: '60px',
            fontWeight: 'bold',
            float: 'left',
            margin: '5px'
        };
        _style.fontWeight = (this.props.active) ? '600' : '200';
        _style.color = (this.props.enabled) ? 'black' : 'grey';
        _style.borderColor = (this.props.enabled) ? 'black' : 'grey';
        const element = this.props.elementInfo;

        return (
            <div style={_style} title={element.name + ' - ' + element.since} onClick={this.onClickFn}>
                {element.code}
            </div>
        );
    }

    onClickFn() {
        let element = this.props.elementInfo;
        if (!element.active && element.enabled) {
            if ('href' in element && typeof element.href === 'string' && element.href !== '') {
                location.href = element.href;
            }
        }
    }
}
