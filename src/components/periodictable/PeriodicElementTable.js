
import React from 'react';
import {BaseComponent} from "../BaseComponent";
import {PeriodicElement} from "./PeriodicElement";

export class PeriodicElementTable extends BaseComponent {
    render() {
        const elements = [
            {
                'code': 'St',
                'name': 'Storytellium',
                'since': 'Discovered 2018',
                'enabled': true,
                'href': 'http://localhost:5001'
            },
            {
                'code': 'Mt',
                'name': 'Marketium',
                'since': 'Discovered 2017',
                'enabled': true,
                'href': 'http://localhost:5002'
            },
            {
                'code': 'Rq',
                'name': 'Requestium',
                'since': 'Discovered 2016',
                'enabled': true,
                'href': 'http://localhost:5003'
            },
            {
                'code': 'Co',
                'name': 'Collectium',
                'since': 'Discovered 2016',
                'enabled': true,
                'href': 'http://localhost:5004'
            },
            {
                'code': 'Pf',
                'name': 'Portfolium',
                'since': 'Discovered 2015',
                'enabled': true,
                'href': 'http://localhost:5005'
            },
            {
                'code': 'Jb',
                'name': 'Jobium',
                'since': 'To be discovered 2018',
                'enabled': false
            },
            {
                'code': 'Tr',
                'name': 'Travelium',
                'since': 'To be discovered 2018',
                'enabled': false
            },
            {
                'code': 'Md',
                'name': 'Modelium',
                'since': 'To be discovered 2018',
                'enabled': false
            },
            {
                'code': 'Fe',
                'name': 'Feedium',
                'since': 'To be discovered 2018',
                'enabled': false
            }
        ];
        let selected = this.props.activeElement || '';
        let withLinks = this.props.withLinks || false;

        return (
            <div>
                {elements.map(function (el) {
                    if (!withLinks && 'href' in el) {
                        delete el['href'];
                    }
                    return (
                        <PeriodicElement key={el.code.toLowerCase()} elementInfo={el} active={el.code === selected}
                                         enabled={el.enabled}/>
                    )
                })}
            </div>
        );
    }
}

